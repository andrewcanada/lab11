#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	
	// TODO
	// Allocate memory for an array of strings (arr).
    char **arr = malloc(10*sizeof(char*));
    char line[20];
    int i = 0;
    int max = 10;
	// Read the dictionary line by line.
    	while(fgets(line, 19, in)){
            char *new = strchr(line, '\n');
            if(new){
                *new = '\0';
            }
    // Allocate memory for the string (str).
        char *str = malloc(sizeof(char)*strlen(line));
	// Copy each line into the string (use strcpy).
        strcpy(str, line);
	// Attach the string to the large array (assignment =).
        arr[i] = str;
        i++;
    // Expand array if necessary (realloc).
        if(i == max)
        {
            max += 10;
            arr = realloc(arr,  max*sizeof(char*));
        }
    }
	// The size should be the number of entries in the array.
	*size = i;
    fclose(in);
	// Return pointer to the array of strings.
	return arr;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}